#!/bin/bash -x

source $(dirname "$0")/polaris_integration_test.conf

usage() {
    cat << EOF
$0: A Small but Useful(tm) utility to run polaris tests on ${TARGETDIR}.

usage: $0 options

OPTIONS:

   -b		Test batch invocation
   -i		Test import + batch
   -c		Clean up any already-downloaded data (/tmp/data*)
   -f		Full pipeline: fetch, learn, viz
   -p [dir]	Use this path for the virtualenv instead of the default ($VENV_DIR)
   -h		This helpful message.

EOF
    exit 1
}

complain_and_die() {
    echo "$*"
    exit 2
}

run_import_cmd() {
    polaris fetch \
	    --import_file "$IMPORTFILE" \
	    --cache_dir "${CACHEDIR}" \
	    LightSail-2 \
	    "${NORMFILE}"
}

copy_normfile_to_where_batch_expects_it() {
    mkdir -p /tmp/polaris/LightSail-2/cache/
    cp "${NORMFILE}" /tmp/polaris/LightSail-2/cache/normalized_frames.json
}

run_batch_cmd() {
    polaris batch --config_file ./polaris/common/polaris_config.json.EXAMPLE
}

clean_data() {
    echo "Cleaning cached files..."
    find "${CACHEDIR}" -depth -user "${USERNAME}" -delete
}

PIP_INSTALL=1
FULL_PIPELINE=0
TEST_BATCH=0
TEST_IMPORT_AND_BATCH=0
while getopts "bcfpi:h" OPTION ; do
    case $OPTION in
	b) TEST_BATCH=1 ;;
        c) clean_data ;;
	f) FULL_PIPELINE=1 ;;
	i) TEST_IMPORT_AND_BATCH=1 ;;
	p) VENV_DIR=$1 ;;
        h) usage ;;
        ?) usage ;;
    esac
    shift
done
shift "$((OPTIND-1))"

if [[ $1 ]] ; then
    cd "$1" || complain_and_die "Can't change directory to ${1}!"
elif [[ -d ${TARGETDIR} ]] ; then
    cd "${TARGETDIR}" || complain_and_die "Can't change directory to ${TARGETDIR}!"
fi
 source ${VENV_DIR}/bin/activate

if [[ TEST_BATCH -eq 1 ]]; then
    run_batch_cmd
    exit $?
fi

if [[ TEST_IMPORT_AND_BATCH -eq 1 ]] ; then
    run_import_cmd || complain_and_die "Problem running import command!"
    copy_normfile_to_where_batch_expects_it || complain_and_die "Problem copying normfile!"
    run_batch_cmd || complain_and_die "Problem running batch command!"
fi

rm "${NORMFILE}"

polaris fetch \
	-s $START_DATE \
	-e $END_DATE \
	--cache_dir "${CACHEDIR}" \
	LightSail-2 \
	"${NORMFILE}"
FETCH_RC=$?
case $FETCH_RC in
    0) FETCH_MSG="Success!" ;;
    *) FETCH_MSG="Fetch failure: exit code $?" ;;
esac

LINE_COUNT=$(jq . "${NORMFILE}" | wc -l | awk '{print $1}')
RC=$?
LINE_MSG="Lines: $LINE_COUNT"
if [[ $LINE_COUNT -gt 0 && $RC -eq 0 ]] ; then
    LINE_MSG="$LINE_MSG 😊"
else
    LINE_MSG="$LINE_MSG 💩"
fi

METADATA_COUNT=$(jq '.metadata | length' "${NORMFILE}")
RC=$?
METADATA_MSG="Metadata: $METADATA_COUNT"
if [[ $METADATA_COUNT -gt 0 && $RC -eq 0 ]] ; then
    METADATA_MSG="$METADATA_MSG 😊"
else
     METADATA_MSG="$METADATA_MSG 💩"
fi

FRAME_COUNT=$(jq '.frames | length' "${NORMFILE}")
RC=$?
FRAME_MSG="Frames: $FRAME_COUNT"
EXPECTED_FRAME_COUNT=12
if [[ $FRAME_COUNT -eq $EXPECTED_FRAME_COUNT && $RC -eq 0 ]]; then
    FRAME_MSG="$FRAME_MSG 😊"
else
    FRAME_MSG="$FRAME_MSG 💩"
fi

polaris learn -g /tmp/new_graph.json "${NORMFILE}"
LEARN_RC=$?
case $LEARN_RC in
    0) LEARN_MSG="Success!" ;;
    *) LEARN_MSG="Learn failure: exit code $?" ;;
esac

NODE_COUNT=$(jq '.nodes | length' "${GRAPHFILE}")
RC=$?
NODE_MSG="Nodes: $NODE_COUNT"
# Somewhat arbitrary threshold
NODE_THRESHOLD=25
if [[ $NODE_COUNT -gt $NODE_THRESHOLD && $RC -eq 0 ]]; then
    NODE_MSG="$NODE_MSG 😊"
else
    NODE_MSG="$NODE_MSG 💩"
fi

DETAILS_MSG="$LINE_MSG $METADATA_MSG $FRAME_MSG $NODE_MSG" #
# Expire in 5000 ms
MSG="Testing done!"
notify-send --expire-time 5000 "$MSG" "$DETAILS_MSG"



if [[ $FULL_PIPELINE -eq 0 ]] ; then
    exit
fi

polaris viz /tmp/new_graph.json
