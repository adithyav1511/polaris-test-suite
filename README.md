# polaris-test-suite

A Small but Useful™ set of shell scripts I use to run integration
tests on [Polaris](https://gitlab.com/crespum/polaris).  Hard-coded
for my laptop, but should be useful for others with a bit of
customization.  At some point, I'd like to turn this into an actual
test suite in the repo.

Usage:

- Edit `polaris_integration_test.conf` and set things appropriately;
  in particular, `LOCAL_REPO` and `TARGETDIR`

- Run `clone_polaris_clean.sh`.  This will:

  - Prepare a fresh clone of Polaris at `TARGETDIR`.  By default, it
    will use the same branch as is checked out at `LOCAL_REPO`.

  - Prepare a fresh virtualenv at `TARGETDIR`

  - Run `invoke_polaris.sh`. By default, this will run `polaris fetch`
    for LightSail-2, then `polaris learn`.

`clone_polaris_clean.sh` and `invoke_polaris.sh` have `--help`
arguments; use them to find more options.

# Shortcomings, TODOs and accusations

- This is *very* customized to how I've been running things.  There
  are a bunch of things that should be broken out into vars that
  aren't.

- This does not account for using `polaris batch` and a config file.

- If this gets much more complicated, it will be worth thinking about
  porting this to Python for ease of maintenance.

# License

LGPL v3, same as Polaris itself.
