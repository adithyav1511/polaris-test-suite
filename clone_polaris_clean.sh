#!/bin/bash -x

TEST_SUITE_ROOT=$(dirname $(realpath "$0"))

source ${TEST_SUITE_ROOT}/polaris_integration_test.conf

usage() {
    cat << EOF
$0: A Small but Useful(tm) utility to clone polaris cleanly, set it up and run tests.

usage: $0 [options] [branch]

OPTIONS:

   -r [arg]	Which repo to use:
		[c]respum
		[d]eckbsd
		[l]ocal
		[s]aintaardvark
      		Default: [l]ocal
   -c		Clean all downloaded data directories.
   -f		Run full pipeline by passing "-f" arg to invoke_polaris.sh
   -b		Run batch operation by passing "-b" argument to invoke_polaris.sh
   -i		Run import + batch operation by passing "-i" argument to invoke_polaris.sh
   -x		Install extra packages
   -h		This helpful message.

NOTES:

- Git remote is *always* set to saintaardvark.

- The repo is always cloned to ${TARGETDIR}.

- ${TARGETDIR} is removed *without* checking.

- If [branch] is not specified, then it's assumed that the currently checked-out branch in the local repo should be cloned.
EOF
    exit 1
}

complain_and_die() {
    echo "$*"
    exit 2
}

install_extra_packages() {
    # If you're going to use watchdog to install other things:
    pip install pytest watchdog argh pyyaml
}

CLEAN_ARG=
INVOKE_POLARIS_ARG=
INSTALL_EXTRA_PACKAGES=no
while getopts "r:cfbixh" OPTION ; do
     case $OPTION in
         r) REPO_ARG=$OPTARG ;;
	 c) CLEAN_ARG="-c" ;;
	 f) INVOKE_POLARIS_ARG="-f" ;;
	 i) INVOKE_POLARIS_ARG="-i" ;;
	 b) INVOKE_POLARIS_ARG="-b" ;;
	 x) INSTALL_EXTRA_PACKAGES="yes" ;;
         h) usage ;;
         ?) usage ;;
     esac
     # shift
done
shift "$((OPTIND-1))"

case $REPO_ARG in
    c*) REPO="git@gitlab.com:crespum/polaris.git" ;;
    d*) REPO="git@gitlab.com:deckbsd/polaris.git" ;;
    l*) REPO=$LOCAL_REPO ;;
    s*) REPO="git@gitlab.com:saintaardvark/polaris.git" ;;
    *)  REPO=$LOCAL_REPO ;;
esac

if [[ $1 ]]; then
    BRANCH="$1"
else
    pushd "$LOCAL_REPO"
    BRANCH=$(git rev-parse --abbrev-ref HEAD)
    echo "Cloning branch I see checked out at ${LOCAL_REPO}: ${BRANCH}"
    popd
fi

rm -rf "${TARGETDIR}"
git clone \
    --single-branch \
    --branch "$BRANCH" \
    "$REPO" \
    "$TARGETDIR" || exit 1
cd "$TARGETDIR" || exit 2

git remote add saintaardvark git@gitlab.com:saintaardvark/polaris.git
git config --add "branch.${BRANCH}.pushRemote" saintaardvark
git fetch -a

python -mvirtualenv .venv
source .venv/bin/activate
pip install -e .
# Make it editable
python setup.py develop
pip install --upgrade pip
pip install tox

if [[ $INSTALL_EXTRA_PACKAGES == "yes" ]]; then
    install_extra_packages
fi
${TEST_SUITE_ROOT}/invoke_polaris.sh "$CLEAN_ARG" "$INVOKE_POLARIS_ARG" "$TARGETDIR"
